package com.canway.book_store.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/")
public class BootStoreController {

    @RequestMapping("/")
    public String home() {
        return "/index.html";
    }

    @RequestMapping("/manage")
    public String manage() {
        return "/manage.html";
    }

    @RequestMapping("/testParam")
    @ResponseBody
    public HashMap<String, String> testParam(@PathParam(value = "param") String param) {
        HashMap<String, String> result = new HashMap<>();
        result.put("message", "This is what you input: " + param);
        return result;
    }

    @RequestMapping(value = "/buyBook", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, String> buyBook(@RequestBody HashMap<String, String> params){
        HashMap<String, String> result = new HashMap<>();
        if (!params.containsKey("book")){
            result.put("message", "Please add book key as post param!");
            return result;
        }
        String book = params.get("book");
        HashMap<String, Integer> bookPrince = new HashMap<>();
        bookPrince.put("Java", 30);
        bookPrince.put("Python" , 25);
        bookPrince.put("C", 35);
        if (bookPrince.containsKey(book)){
            Integer price = bookPrince.get(book);
            result.put("message", "OK, you need to pay for " + book + ".");
            result.put("price", price + " Yuan");
        }else {
            result.put("message", "There is no book names " + book + ", you can choose Java, Python or C");
            result.put("price", "No price");
        }
        return result;
    }

}

